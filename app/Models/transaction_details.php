<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TransactionDetail extends Model
{
    use HasFactory;
    /**
     * fillable
     * 
     * @var array
     */
    protected $fillable =[
        'transaction_id','product_id','qty','price'
    ];
    /**
     * transactions
     * 
     * @return void
     */
    public function transactions()
    {
        return $this->BelongTo(Transactions::class);
    }
    /**
     * product
     * 
     * @return void
     */
    public function product()
    {
        return $this->BelongTo(Products::class);
    }
}
