<?php

namespace App\Models;
use carbon\carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Casts\Attribute;

class category extends Model
{
    use HasFactory;
    /**
     * fillable
     * @var array
     */
    protected $fillable =[
        'image','name','description'
    ];

    /**
     * products
     * 
     * @return void
     */
    public function products()
    {
        return $this->HasMany(products::class);
    }
    /**
     * image
     * 
     * @return attribute
     */
    protected function image(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => asset('/storage/products' . $value),
        );
    }
}
