<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Casts\Attribute;
class product extends Model
{
    use HasFactory;
    /**
     * fillable
     * @var array
     */
    protected $fillable =[
        'image','barcode','title','description','buy_price','sell_price','stock'
    ];

    /**
     * categories
     * 
     * @return void
     */
    public function categories()
    {
        return $this->BelongTo(categories::class);
    }

    /**
     * details
     * 
     * @return void
     */
    public function details()
    {
        return $this->HasMany(transactions_detail::class);
    }
}
