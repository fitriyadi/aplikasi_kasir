<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class cart extends Model
{
    use HasFactory;
    /**
     * fillable
     * @var array
     */
    protected $fillable =[
        'cashier_id','product_id','qty'
    ];
    /**
     * products
     * 
     * @return void
     */
    public function products()
    {
        return $this->BelongTo(Products::class);
    }
}
